import json
import logging
import sys

from email.message import EmailMessage
from smtplib import SMTP, SMTPException

from dynaconf import settings
from kafka import KafkaConsumer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic



log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", 
                          datefmt="%Y-%m-%d - %H:%M:%S")
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

log.addHandler(ch)

log.debug("running with config: %s", settings.as_dict())

# create kafka listener
consumer = KafkaConsumer(settings.KAFKA.topic,
                            group_id=settings.KAFKA.group,
                            value_deserializer=lambda m: json.loads(
                                m.decode('utf-8')),
                            bootstrap_servers=settings.KAFKA.bootstrapservers,
                            auto_offset_reset="earliest")

def create_topic():
    admin_client = KafkaAdminClient(bootstrap_servers=settings.KAFKA.bootstrapservers, client_id='test')
    topic_metadata = admin_client.list_topics()
    
    if settings.KAFKA.topic not in topic_metadata:
        #yLogger.info("Topics werden erstellt")
        topic_list = []
        topic_list.append(NewTopic(name=settings.KAFKA.topic, num_partitions=10, replication_factor=1))
        admin_client.create_topics(new_topics=topic_list, validate_only=False)

# status info
log.debug("created consumer. waitiung for messages...")

create_topic()

# consume
for message in consumer:
    # message info
    log.debug("processing booking: %s", str(message))

    # create mail
    msg = EmailMessage()
    msg.set_content(str(message.value))
    msg['Subject'] = 'Your Order has been booked'
    msg['From'] = settings.SMTP.sender
    msg['To'] = settings.SMTP.recipient

    # send mail via smtp
    try:
        smtpObj = SMTP(settings.SMTP.host, port=settings.SMTP.port)
        smtpObj.send_message(msg)
        log.info("Successfully sent email: to %s", msg['To'])
    except SMTPException as exception:
        log.error("Error: unable to send email: %s \n %s",
                        str(msg), str(exception))

